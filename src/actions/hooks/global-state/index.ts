import {
  IChangeState,
  IListener,
  IListeners,
  IListenStateChange,
  IRemoveListener,
  IState,
} from '@interfaces/actions/global-state';

const listeners: IListeners = [];

export const listenStateChange =
  (id: string, setState: (state: IState) => void, state: IState): IListenStateChange =>
  (key: string): IState => {
    removeListener(id)();
    const listener: IListener = [id, key, setState];
    listeners.push(listener);

    return state;
  };

export const changeState: IChangeState = (key: string, value: IState): void => {
  listeners.forEach(([n, k, c]: IListener) => {
    if (!!n && k === key) {
      c(value);
    }
  });
};

export const removeListener =
  (id: string): IRemoveListener =>
  (): void => {
    const index = listeners.findIndex(([n]) => n === id);
    if (index !== -1) {
      listeners.splice(index, 1);
    }
  };
