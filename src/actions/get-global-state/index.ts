import { useGlobalState } from '@hooks/global-state';
import { IGetGlobalState } from '@interfaces/hooks/global-state';

export const getGlobalState: IGetGlobalState = () => {
  const id = '' + Math.random().toString(36).substr(2, 9);

  return () => useGlobalState(id);
};
