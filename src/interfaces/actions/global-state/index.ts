export type IState = string | number | Record<string, string> | Record<string, unknown> | null;
export type ICallback = (value: IState) => void;
export type IListener = [string, string, ICallback];
export type IListeners = IListener[];

export type IListenStateChange = (key: string) => IState;
export type IChangeState = (key: string, value: IState) => void;
export type IRemoveListener = () => void;
