import { IChangeState, IListenStateChange, IRemoveListener } from '@interfaces/actions/global-state';

export type IGlobalState = [IListenStateChange, IChangeState, IRemoveListener];
export type IUseGlobalState = (id: string) => IGlobalState;
export type IGetGlobalState = () => () => IGlobalState;
