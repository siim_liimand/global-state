import { useState } from 'preact/hooks';

import { changeState, listenStateChange, removeListener } from '@actions/hooks/global-state';
import { IUseGlobalState } from '@interfaces/hooks/global-state';
import { IState } from '@interfaces/actions/global-state';

export const useGlobalState: IUseGlobalState = (id) => {
  const [state, setState] = useState<IState>(null);

  return [listenStateChange(id, setState, state), changeState, removeListener(id)];
};
